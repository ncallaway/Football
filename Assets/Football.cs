﻿using UnityEngine;
using System.Collections;

public class Football : MonoBehaviour {

    public static readonly float ROTATION_SPEED = 40;

    public AudioSource ballHitAudio;
    public AudioSource incompleteAudio;

    private bool isThrown = false;
    private bool isComplete;
    private bool isIncomplete;

    public bool IsThrown { get { return isThrown; } }
    public bool WasComplete { get { return isComplete; } }
    public bool WasIncomplete { get { return isIncomplete; } }

    private Transform originalParent;
    private Rigidbody fbRigidbody;

    void Start() {
        originalParent = transform.parent;
        fbRigidbody = GetComponent<Rigidbody>();
    }

    public void FloatAt(Vector3 position, Quaternion orientation) {
        transform.position = position;
        transform.rotation = orientation;
        fbRigidbody.isKinematic = true;
    }

    public void AttachTo(Transform parent, Vector3 localPosition, Quaternion localRotation) {

        transform.parent = parent;
        fbRigidbody.isKinematic = true;
        transform.localPosition = localPosition;
        transform.localRotation = localRotation;
    }

    public void Throw(Vector3 launchVelocity) {
        transform.parent = originalParent;
        fbRigidbody.isKinematic = false;
        fbRigidbody.velocity = launchVelocity;

        fbRigidbody.AddRelativeTorque(Vector3.left*ROTATION_SPEED, ForceMode.VelocityChange);

        isThrown = true;
    }

    public void Reset()
    {
        isThrown = false;
        isComplete = false;
        isIncomplete = false;
        transform.parent = originalParent;
    }

    public void Completed()
    {
        isComplete = true;
        isThrown = false;
        ballHitAudio.Play();
    }

    // Play some noises, see if we're incomplete.
    void OnCollisionEnter(Collision collision)
    {
        if (isThrown) {
            ballHitAudio.Play();

            if (collision.collider.GetComponent<Ground>() != null) {
                isIncomplete = true;
                isThrown = false;

                incompleteAudio.PlayDelayed(0.5f);
            }
        }
    }
}
