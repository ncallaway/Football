﻿using UnityEngine;
using System.Collections;

public class ControllerHaptics : MonoBehaviour {

	private SteamVR_Controller.Device controller { get { return SteamVR_Controller.Input((int)trackedObj.index); } }
    private SteamVR_TrackedObject trackedObj;

	// Use this for initialization
	void Start () {
		trackedObj = GetComponent<SteamVR_TrackedObject>();
	}

	public void Vibrate() {
		Vibrate(0.25f, 0.5f, 0.05f, false);
	}

	public void Vibrate(float duration) {
		Vibrate(duration, 0.5f, 0.05f, false);
	}

	public void Vibrate(float duration, float strength) {
		Vibrate(duration, strength, 0.05f, false);
	}

	public void Vibrate(float duration, float strength, float vibration) {
		Vibrate(duration, strength, vibration, false);
	}

	public void Vibrate(float duration, float strength, float vibration, bool fade) {
		StartCoroutine(Pulse(duration, (int)(3999*strength), vibration, fade));
	}

	// Haptic Feedback
    private IEnumerator Pulse(float duration, int hapticPulseStrength, float pulseInterval, bool fade)
    {
        float initialDuration = duration;

        if (pulseInterval <= 0)
        {
            yield break;
        }

        while (duration > 0)
        {
            float fadeFactor = fade ? (duration / initialDuration) : 1;
            controller.TriggerHapticPulse((ushort)(hapticPulseStrength*fadeFactor));

            yield return new WaitForSeconds(pulseInterval);
            duration -= pulseInterval;
        }
    }
}
