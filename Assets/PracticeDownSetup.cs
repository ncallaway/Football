﻿using UnityEngine;
using System.Collections;

public class PracticeDownSetup : MonoBehaviour {

	private PracticeGameManager manager;

	public Transform lineOfScrimmage;
	public RouteRunner runner;
	public Football football;
	public Vector3 footballOffset = new Vector3(0, 0.5f, -0.5f);

	private void Start() {
		manager = GetComponent<PracticeGameManager>();
		Events.EventBus.Listen(Events.EventType.HOT_ROUTE_STATE_CHANGE, OnStateChange);
	}

	private void OnStateChange(Events.Event e) {
		Events.HotRouteStateChangeEvent hre = (Events.HotRouteStateChangeEvent)e;
		GameState state = hre.HotRouteState;

		if (state.CurrentStatus == GameState.GameStatus.BEFORE_ROUTE) {
			SetupRunner();
			SetupLineOfScrimmage();
			SetupFootball();
		}
	}

	private void SetupRunner() {
		runner.Reset();
		Debug.Log("SETTING UP RUNNER: " + manager.HotRouteState.CurrentRoute);
		runner.SetRoute(manager.HotRouteState.CurrentRoute);
	}

	private void SetupLineOfScrimmage() {
		Vector3 losPosition = lineOfScrimmage.transform.position;
		losPosition.z = manager.HotRouteState.CurrentFieldPosition;

		lineOfScrimmage.transform.position = losPosition;
	}

	private void SetupFootball() {
		football.FloatAt(footballOffset + lineOfScrimmage.transform.position, Quaternion.identity);
        football.Reset();
	}
}
