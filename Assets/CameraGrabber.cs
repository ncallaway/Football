﻿using UnityEngine;
using Events;
using System.Collections;

public class CameraGrabber : MonoBehaviour {
	private const int LEFT_MOUSE_BUTTON = 0;

	// Public Parameters
	public Vector3 attachmentPosition = new Vector3 (0.3f, -0.2f, 0.5f);
	public Quaternion attachmentOrientation = Quaternion.Euler (270, 90, 0);
	public float launchSpeed = 20;
	public float rotationSpeed = 10;

	// Private State
	private Football attachedFootball;

	private void Update () {
		if (Input.GetMouseButtonDown (LEFT_MOUSE_BUTTON) && this.attachedFootball == null) {
			// Attempt to grab football
			RaycastHit hit;
			Ray ray = GetComponent<Camera>().ScreenPointToRay(Input.mousePosition  + transform.forward * 0.2f);
			if (Physics.Raycast(ray, out hit)) {
				Football football = hit.collider.GetComponent<Football>();

				// We have an attached Football, grab it!
				if (football != null) {
					attachedFootball = football;
					EventBus.Dispatch(Events.Event.PickupFootball(attachedFootball));
					attachedFootball.AttachTo(transform, attachmentPosition, attachmentOrientation);
				}
			}
		} else if (Input.GetMouseButtonDown (LEFT_MOUSE_BUTTON) && this.attachedFootball != null) {
			// Throw the football
			Vector3 launchVelocity = transform.forward.normalized * launchSpeed;

			attachedFootball.Throw(launchVelocity);
			attachedFootball = null;
		}
	}
}
