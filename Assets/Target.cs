﻿using UnityEngine;

public class Target : MonoBehaviour {

    public Color defaultColor;
    public Color completeColor;
    public Color incompleteColor;

    private void Start() {
        Events.EventBus.Listen(Events.EventType.NEW_DOWN, OnNewDown);
        Events.EventBus.Listen(Events.EventType.COMPLETE_PASS, OnComplete);
        Events.EventBus.Listen(Events.EventType.INCOMPLETE_PASS, OnIncomplete);
    }

    private void OnTriggerEnter(Collider collider)
    {
        Football f = collider.GetComponent<Football>();

        if (f != null && f.IsThrown)
        {
            f.Completed();
        }
    }

    private void OnComplete(Events.Event e) {
        GetComponent<Renderer>().material.color = completeColor;
    }

    private void OnIncomplete(Events.Event e) {
        GetComponent<Renderer>().material.color = incompleteColor;
    }

    private void OnNewDown(Events.Event e) {
        GetComponent<Renderer>().material.color = defaultColor;
    }
}
