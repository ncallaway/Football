﻿using UnityEngine;
using System.Collections;

namespace Events {
	public enum EventType {
		PICKUP_FOOTBALL,
		MENU_BUTTON,

		NEW_DOWN,
		SNAP,

		HOT_ROUTE_STATE_CHANGE,
		COMPLETE_PASS,
		INCOMPLETE_PASS,
		TOUCHDOWN
	}

	public abstract class Event {
		public EventType Type { get { return type; } }
		private EventType type;

		protected Event(EventType type) {
			this.type = type;
		}

		public static PickupFootballEvent PickupFootball(Football football) {
			return new PickupFootballEvent(football);
		}

		public static MenuButtonEvent MenuButton() { return new MenuButtonEvent(); }

		public static HotRouteStateChangeEvent HotRouteStateChange(GameState hotRouteState) {
			return new HotRouteStateChangeEvent(hotRouteState);
		}

		public static NewDownEvent NewDown() { return new NewDownEvent(); }
		public static SnapEvent Snap() { return new SnapEvent(); }

		public static CompletePassEvent CompletePass() { return new CompletePassEvent(); }
		public static IncompletePassEvent IncompletePass() { return new IncompletePassEvent(); }
		public static TouchdownEvent Touchdown() { return new TouchdownEvent(); }
	}

	public class PickupFootballEvent : Event {
		public Football Football { get { return football; } }
		private Football football;

		public PickupFootballEvent(Football football) : base(EventType.PICKUP_FOOTBALL) {
			this.football = football;
		}
	}

	public class MenuButtonEvent : Event { public MenuButtonEvent() : base(EventType.MENU_BUTTON) {} }

	public class HotRouteStateChangeEvent : Event {
		public GameState HotRouteState { get { return hotRouteState; } }
		private GameState hotRouteState;

		public HotRouteStateChangeEvent(GameState hotRouteState) : base(EventType.HOT_ROUTE_STATE_CHANGE) {
			this.hotRouteState = hotRouteState;
		}
	}

	public class NewDownEvent : Event { public NewDownEvent() : base(EventType.NEW_DOWN) {} }
	public class SnapEvent : Event { public SnapEvent() : base(EventType.SNAP) {} }

	public class CompletePassEvent : Event { public CompletePassEvent() : base(EventType.COMPLETE_PASS) {} }
	public class IncompletePassEvent : Event { public IncompletePassEvent() : base(EventType.INCOMPLETE_PASS) {} }
	public class TouchdownEvent : Event { public TouchdownEvent() : base(EventType.TOUCHDOWN) {} }
}