﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace Events {
	public class EventBus : MonoBehaviour {
		private Dictionary<EventType, List<Action<Event>>> registeredListeners = new Dictionary<EventType, List<Action<Event>>>();

		private void PerformListen(EventType type, Action<Event> callback) {
			if (!registeredListeners.ContainsKey(type)) {
				registeredListeners[type] = new List<Action<Event>>();
			}

			registeredListeners[type].Add(callback);
		}

		private void PerformDispatch(Event e) {
			if (registeredListeners.ContainsKey(e.Type)) {
				List<Action<Event>> callbacks = registeredListeners[e.Type];

				foreach (Action<Event> callback in callbacks) {
					callback(e);
				}
			}
		}

		public static void Listen(EventType type, Action<Event> callback) {
			Debug.Log("Does an event bus exist? " + Bus());
			Bus().PerformListen(type, callback);
		}

		public static void Dispatch(Event e) {
			Bus().PerformDispatch(e);
		}

		private static EventBus Bus() {
			GameObject[] rootGameObjects = UnityEngine.SceneManagement.SceneManager.GetActiveScene().GetRootGameObjects();

			EventBus bus = null;
			int busCount = 0;
			foreach(GameObject rootGameObj in rootGameObjects) {
				EventBus objBus = rootGameObj.GetComponent<EventBus>();

				if (objBus != null) {
					bus = objBus;
					busCount++;
				}
			}

			if (busCount > 1) {
				Debug.LogError("THERE WERE MORE THAN 1 EVENT BUSSES ACTIVE IN THE SCENE. BAD THINGS WILL HAPPEN NOW.");
			}

			if (busCount < 1 || bus == null) {
				Debug.LogError("THERE WEREN'T ANY EVENT BUSSES ACTIVE IN THE SCENE. BAD THINGS WILL HAPPEN NOW.");
			}

			return bus;
		}
	}
}
