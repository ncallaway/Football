﻿using UnityEngine;

public class Notification {

	public Notification(string message, Color color, int fontSize, float duration) {
		this.message = message;
		this.color = color;
		this.fontSize = fontSize;
		this.duration = duration;
	}

	public string message;
	public int fontSize;
	public Color color;

	public float duration;
}
