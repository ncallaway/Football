﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class HUDNotificationManager : MonoBehaviour {

	public Color successColor;
	public Color warningColor;
	public Color infoColor;
	public Color dangerColor;

	public int fontSize = 48;
	public int largeFontSize = 60;

	public float duration = 0.75f;

	public NotificationAnimation notificationPrefab;

	public float timeBetweenNotifications = 0.5f;

	private float lastNotificationStartTime;

	private Queue<Notification> notificationQueue = new Queue<Notification>();

	// Use this for initialization
	private void Start () {
		Events.EventBus.Listen(Events.EventType.COMPLETE_PASS, OnComplete);
		Events.EventBus.Listen(Events.EventType.INCOMPLETE_PASS, OnIncomplete);
		Events.EventBus.Listen(Events.EventType.TOUCHDOWN, OnTouchdown);
	}

	private void Update() {
		if (notificationQueue.Count > 0) {
			if (Time.time - lastNotificationStartTime > timeBetweenNotifications) {
				StartNotification(notificationQueue.Dequeue());

				lastNotificationStartTime = Time.time;
			}
		}
	}

	private void OnComplete(Events.Event e) {
		EnqueueNotification(new Notification("FUCKING NICE", infoColor, fontSize, duration));
	}

	private void OnIncomplete(Events.Event e) {
		EnqueueNotification(new Notification("FUCKING TERRIBLE!", warningColor, fontSize, duration));
		EnqueueNotification(new Notification("EXTREMELY TERRIBLE!", warningColor, fontSize, duration));
		EnqueueNotification(new Notification("SAD!", dangerColor, largeFontSize, duration*1.5f));
	}

	private void OnTouchdown(Events.Event e) {
		EnqueueNotification(new Notification("TOUCHDOWN, BITCH!", successColor, largeFontSize, duration*2));
	}

	private void EnqueueNotification(Notification notification) {
		notificationQueue.Enqueue(notification);
	}

	private void StartNotification(Notification notification) {
		NotificationAnimation animation = (NotificationAnimation)Instantiate(notificationPrefab);
		
		// Unfuck instantiation
		animation.transform.SetParent(transform, false);
		// Vector3 localScale = animation.transform.localScale;
		// Quaternion localRotation = animation.transform.localRotation;
		// animation.transform.parent = transform;
		// animation.transform.localScale = localScale;
		// animation.transform.localRotation = localRotation;

		// Set notification properties
		Text notificationText = animation.GetComponent<Text>();
		notificationText.text = notification.message;
		notificationText.fontSize = notification.fontSize;
		animation.GetComponent<Outline>().effectColor = notification.color*.85f;
		notificationText.color = notification.color;

		animation.animationDuration = notification.duration;		
	}
}
