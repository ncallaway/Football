﻿using UnityEngine;
using UnityEngine.UI;

public class NotificationAnimation : MonoBehaviour {

	public float endHeight = 24.5f;
	public float startHeight = 0;

	public float animationDuration = 1;	

	private float animationStartTime;
	private RectTransform rectTransform;
	private float startAlpha;
	private Text text;

	// TODO: Don't actually do this
	private static float s = 1.25f;

	private void Start () {
		animationStartTime = Time.time;

		rectTransform = GetComponent<RectTransform>();

		text = GetComponent<Text>();

		startAlpha = text.color.a;
	}
	
	private void Update () {
		// Calculate how farinto the animation we are
		float elapsed = Time.time - animationStartTime;
		float elapsedRatio = Mathf.Clamp01(elapsed / animationDuration);

		// Remap curve to totally sweet better curve
		float mappedRatio = EasingCurve(elapsedRatio);

		// Set position and alpha of text based on animation duration
		float yPosition = startHeight*(1-mappedRatio) + endHeight*(mappedRatio);
		rectTransform.localPosition = new Vector3(0, yPosition, 0);

		Color currentColor = text.color;
		currentColor.a = (1-mappedRatio)*startAlpha;
		text.color = currentColor;

		// Kill ourselves if the animation is done
		if (elapsedRatio >= .99f) {
			GameObject.Destroy(gameObject);
		}
	}

	private float EasingCurve(float t) {
		// float eased = 0;
		// if (t > .25f) {
		// 	t = (t-0.25f) / 0.75f;
		// 	eased = BackIn(t);
		// }

		return BackIn(t);
	}

		
	public static float BackIn (float k) {
		return k*k*((s + 1f)*k - s);
	}
}
