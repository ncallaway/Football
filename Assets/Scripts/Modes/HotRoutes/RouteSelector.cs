using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;

/**
 * RouteSelector for the HotRoutes Mode
 */
public static class RouteSelector {
    private static readonly float DIFFICULTY_CUTOFF = 5;
    private static System.Random random = new System.Random();

    private static Route[] allRoutes = new Route[] {
		new ShortOut(),
		new ShortPost(),
		new ShallowDrag(),
		new ShortIn(),
        new ShortHook(),
        new ShortCorner(),
        new ShortFade(),

        new MediumOut(),
		new MediumPost(),
		new MediumDrag(),
		new MediumIn(),
        new MediumHook(),
        new MediumCorner(),
        new MediumFade(),

        new DeepOut(),
		new DeepPost(),
		new DeepIn(),
        new DeepHook(),
        new DeepCorner(),
        new DeepFade(),
	};

    /**
     * Select an appropriate route at random given the current distance to the endzone and
     * the current score.
     * The route shouldn't be too long given the distance to the endzone, but if there's a long
     * distance to go, longer routes should be preferred. Routes with a higher difficulty should
     * be selected as more touchdowns are scored.
     */
    public static Route SelectRoute(float distanceToEndzone, float difficulty) {
        Debug.Log("Selecting route for difficulty: " + difficulty);

        List<Route> eligibleRoutes = EligibleRoutes(distanceToEndzone, difficulty);

        // Build our normalized weights
        var difficultyDeltas = eligibleRoutes.Select(r => Mathf.Abs(r.Difficulty - difficulty));
        var invertedDeltas = difficultyDeltas.Select(d => DIFFICULTY_CUTOFF - d);
        var sumDeltas = invertedDeltas.Sum();
        var normalizedWeights = invertedDeltas.Select(d => d / sumDeltas);

        // Select the random value
        float randomValue = (float)random.NextDouble();

        // Find the weighted random index
        float calculatedValue = 0;
        int index = 0;
        foreach(float weight in normalizedWeights) {
            calculatedValue += weight;
            if (calculatedValue >= randomValue) {
                return eligibleRoutes[index];
            }
            index++;
        }

        // Emergency fallback if there were no eligible routes for some reason.
        int randomIndex = random.Next(allRoutes.Length);
        return allRoutes[randomIndex];
    }

    private static List<Route> EligibleRoutes(float distanceToEndzone, float difficulty) {
        List<Route> eligibleRoutes = new List<Route>();

        // Iterate through the routes and add the eligible routes to the list
        foreach (Route route in allRoutes) {
            if (IsRouteEligible(route, distanceToEndzone, difficulty)) {
                eligibleRoutes.Add(route);
            }
        }

        return eligibleRoutes;
    }

    private static bool IsRouteEligible(Route route, float distanceToEndzone, float difficulty) {
        return (route.Difficulty < difficulty + DIFFICULTY_CUTOFF && route.MinimumDistance < distanceToEndzone);
    }
}