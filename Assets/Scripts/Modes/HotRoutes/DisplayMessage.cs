using System;

public static class DisplayMessage {
    public static String GetMessage(GameState state) {
        switch(state.CurrentStatus) {
            case GameState.GameStatus.BEFORE_GAME:
                return BeforeGameMessage(state);
            case GameState.GameStatus.BEFORE_ROUTE:
                return BeforeRouteMessage(state);
            case GameState.GameStatus.RUNNING_ROUTE:
                return RunningRouteMessage(state);
            case GameState.GameStatus.AFTER_ROUTE:
                return AfterRouteMessage(state);
            case GameState.GameStatus.GAME_OVER:
                return GameOverMessage(state);
            default:
                return UnknownStateMessage(state);
        }
    }

    private static String BeforeGameMessage(GameState state) {
        return "Welcome to the THUNDERDOME"; 
    }

    public static String BeforeRouteMessage(GameState state) {
        if (state.Attempts > 0) {
            if (state.LastPassTouchdown) {
                return "TOUCHDOWN!";
            }
            else if (state.LastPassComplete) {
                return "Complete!";
            } else {
                return "Incomplete";
            }
        } else {
            return "Welcome to the THUNDERDOME";
        }
    }

    public static String RunningRouteMessage(GameState state) {
        return "";
    }

    public static String AfterRouteMessage(GameState state) {
        if (state.Attempts > 0) {
            if (state.LastPassTouchdown) {
                return "TOUCHDOWN!";
            }
            else if (state.LastPassComplete) {
                return "Complete!";
            } else {
                return "Incomplete";
            }
        } else {
            return "Welcome to the THUNDERDOME";
        }
    }

    public static String GameOverMessage(GameState state) {
        return "Game Over.\nPress MENU to try again.";
    }

    public static String UnknownStateMessage(GameState state) {
        return "ERR UNKNOWN STATE";
    }
}