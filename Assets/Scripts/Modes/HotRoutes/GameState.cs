using UnityEngine;

/**
 * The current GameState for the HotRoute game mode
 */
public class GameState {

    public enum GameStatus {
        BEFORE_GAME,
        BEFORE_ROUTE,
        RUNNING_ROUTE,
        AFTER_ROUTE,
        GAME_OVER
    }

    public static readonly float ENDZONE = 109;

    public GameStatus CurrentStatus { get { return currentStatus; } }
    public Route CurrentRoute { get { return currentRoute; } }

    public float Difficulty { get { return difficulty; } }

    public int CompletePasses { get { return completePasses; } }
    public int IncompletePasses { get { return incomplePasses; } }
    public int Attempts { get { return completePasses+incomplePasses; } }
    public bool LastPassComplete { get { return lastPassComplete; } }
    public bool LastPassTouchdown { get { return lastPassTouchdown; } }

    public float CurrentFieldPosition { get { return currentFieldPosition; } }
    public float PreviousFieldPosition { get { return previousFieldPosition; } }
    public float DistanceTraveled { get { return distanceTraveled; } }

    public float DistanceToEndzone { get { return ENDZONE - currentFieldPosition; } }

    public int TouchdownsScored { get { return touchdownsScored; } }
    public int Score { get { return touchdownsScored*7; } }

    public int DropsRemaining { get { return dropsRemaining; } }
    public bool IsGameOver { get { return currentStatus == GameStatus.GAME_OVER; } }

    private GameStatus currentStatus;
    private Route currentRoute;

    private float difficulty;

    private int completePasses;
    private int incomplePasses;
    private bool lastPassComplete;
    private bool lastPassTouchdown;

    private float currentFieldPosition;
    private float previousFieldPosition;
    private float distanceTraveled;

    private int touchdownsScored;
    private int dropsRemaining;

    public GameState() {
        ResetGameState();
    }

    /**
     * Reset this GameState so it can be used for a new game.
     */
    public void ResetGameState() {
        currentStatus = GameStatus.BEFORE_GAME;

        completePasses = 0;
        incomplePasses = 0;

        currentFieldPosition = 90;
        previousFieldPosition = currentFieldPosition;

        touchdownsScored = 0;
        dropsRemaining = 3;

        difficulty = 1;

        currentRoute = null;
    }

    public void RouteStarted() {
        currentStatus = GameStatus.RUNNING_ROUTE;
        DispatchStateChange();
    }

    public void RouteSetup(Route route) {
        this.currentRoute = route;
        this.currentStatus = GameStatus.BEFORE_ROUTE;
        DispatchStateChange();
    }

    public void AddCompletePass(float fieldPosition) {
        lastPassTouchdown = false;
        lastPassComplete = true;
        completePasses++;
        previousFieldPosition = currentFieldPosition;
        currentFieldPosition = fieldPosition;

        distanceTraveled += Mathf.Min(currentFieldPosition, ENDZONE) - previousFieldPosition;

        currentStatus = GameStatus.AFTER_ROUTE;
        DispatchStateChange();
    }

    public void IncreaseDifficulty(float deltaDifficulty) {
        difficulty += deltaDifficulty;
        DispatchStateChange();
    }

    public void AddIncompletePass() {
        lastPassTouchdown = false;
        lastPassComplete = false;
        incomplePasses++;
        dropsRemaining--;
        previousFieldPosition = currentFieldPosition;

        currentStatus = dropsRemaining <= 0 ? GameStatus.GAME_OVER : GameStatus.AFTER_ROUTE;
        DispatchStateChange();
    }

    public void AddTouchdown() {
        lastPassTouchdown = true;
        touchdownsScored++;
        DispatchStateChange();
    }

    public void AddDropRemaining() {
        dropsRemaining++;
        DispatchStateChange();
    }

    public void StartNewDriveFrom(float fieldPosition) {
        currentFieldPosition = fieldPosition;
        previousFieldPosition = fieldPosition;
    }

    private void DispatchStateChange() {
        Events.EventBus.Dispatch(Events.Event.HotRouteStateChange(this));
    }
}