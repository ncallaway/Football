﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(Image))]
public class ScoreboardBackgroundFlash : MonoBehaviour {


	public float flashDurationSeconds = 0.5f;

	private float? animationStartTime = null;
	private Image background;
	private int numberOfFlashes;

	// Use this for initialization
	void Start () {
		background = GetComponent<Image>();

		Events.EventBus.Listen(Events.EventType.COMPLETE_PASS, OnComplete);
		Events.EventBus.Listen(Events.EventType.TOUCHDOWN, OnTouchdown);
	}

	/* Start the background flash animation */
	public void FlashBackground(int numberOfFlashes) {
		this.animationStartTime = Time.time;
		this.numberOfFlashes = numberOfFlashes;
	}

	// Update is called once per frame
	void Update () {
		if (!animationStartTime.HasValue) { return; }

		float elapsedTime = Time.time - animationStartTime.Value;
		float totalDuration = flashDurationSeconds*numberOfFlashes;
		float t = elapsedTime / totalDuration;

		if (t > 1) {
			SetBackgroundAlpha(0);
			animationStartTime = null;
		} else {
			float mappedT = t*2*Mathf.PI*numberOfFlashes;
			float backgroundAlpha = (-Mathf.Cos(mappedT) + 1) / 2;
			SetBackgroundAlpha(backgroundAlpha);
		}
	}

	private void SetBackgroundAlpha(float alpha) {
		Color bgColor = background.color;
		bgColor.a = alpha;
		background.color = bgColor;
	}

	private	void OnComplete(Events.Event e) {
		FlashBackground(2);
	}

	private void OnTouchdown(Events.Event e) {
		FlashBackground(5);
	}
}
