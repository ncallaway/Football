﻿using UnityEngine;
using UnityEngine.UI;

public class Scoreboard : MonoBehaviour {
	public Text scoreValue;
	public Image dropsGraphic;
	public Text largeMessage;
	public Text routeLabel;

	public Text attemptsValue;

	private void Start() {
		Events.EventBus.Listen(Events.EventType.HOT_ROUTE_STATE_CHANGE, OnStateChange);
	}

	private void OnStateChange(Events.Event e) {
		Events.HotRouteStateChangeEvent hre = (Events.HotRouteStateChangeEvent)e;
		GameState state = hre.HotRouteState;

		if (scoreValue) {
			scoreValue.text = state.Score.ToString();
		}

		if (dropsGraphic) {
			dropsGraphic.rectTransform.sizeDelta = new Vector2(125*state.DropsRemaining, 125);
		}
		
		if (attemptsValue) {
			attemptsValue.text = state.DropsRemaining.ToString();
		}
		
		if (largeMessage) {
			largeMessage.text = DisplayMessage.GetMessage(state);
		}
		
		if (routeLabel) {
			if (state.CurrentStatus == GameState.GameStatus.BEFORE_ROUTE) {
				routeLabel.text = state.CurrentRoute.Name.ToUpper();
			} else {
				routeLabel.text = "";
			}
		}
	}
}


