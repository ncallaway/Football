﻿using UnityEngine;
using System.Collections;

public class VRModeManager : MonoBehaviour {

	public Transform kmCamera;
	public bool useVR = true;

	void Start () {
		if (useVR) {
			gameObject.SetActive(true);
			kmCamera.gameObject.SetActive(false);
		} else {
			gameObject.SetActive(false);
			kmCamera.parent = transform.parent;
			kmCamera.gameObject.SetActive(true);
		}
	}
}
