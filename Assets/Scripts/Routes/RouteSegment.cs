﻿using UnityEngine;

/**
 * A route segment is a single step in a route to be run
 */
public class RouteSegment {

	public enum SegmentType {
		RUN,
		HANDS_ON,
		HANDS_OFF,
		WAIT
	}

	private RouteSegment(SegmentType type) {
		this.type = type;
	}

	private RouteSegment(SegmentType type, float waitDuration) {
		this.type = type;
		this.waitDuration = waitDuration;
	}

	private RouteSegment(SegmentType type, Vector3 targetOffset) {
		this.type = type;
		this.targetOffset = targetOffset;
	}

	public SegmentType Type { get { return type; } }
	public Vector3 TargetOffset { get { return targetOffset; } }
	public float WaitDuration { get { return waitDuration; } }

	private SegmentType type;
	private Vector3 targetOffset;
	private float waitDuration;

	public static RouteSegment Run(Vector3 offset) {
		return new RouteSegment(SegmentType.RUN, offset);
	}

	public static RouteSegment HandsOn() {
		return new RouteSegment(SegmentType.HANDS_ON);
	}

	public static RouteSegment HandsOff() {
		return new RouteSegment(SegmentType.HANDS_OFF);
	}

	public static RouteSegment Wait(float waitDuration) {
		return new RouteSegment(SegmentType.WAIT, waitDuration);
	}
}
