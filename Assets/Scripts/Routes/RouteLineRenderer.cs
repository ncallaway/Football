﻿using UnityEngine;
using System.Collections.Generic;

[RequireComponent(typeof(RouteRunner))]
[RequireComponent(typeof(LineRenderer))]
public class RouteLineRenderer : MonoBehaviour {

	private RouteRunner routeRunner;
	private LineRenderer lineRenderer;

	// Use this for initialization
	void Start () {
		routeRunner = GetComponent<RouteRunner>();
		lineRenderer = GetComponent<LineRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
		lineRenderer.useWorldSpace = true;

		/* Get all sets of "waypoints" for the route */
		List<Vector3> waypoints = routeRunner.Waypoints;

		/* Set the number of points the LineRenderer will draw */
		if (waypoints.Count == 0) {
			lineRenderer.SetVertexCount(0);
		} else {
			lineRenderer.SetVertexCount(waypoints.Count + 1);
		}
			
		if (waypoints.Count > 0) {
			/* Set the first point to where the RouteRunner currently is */
			lineRenderer.SetPosition(0, transform.position + transform.forward*0.2f);

			/* Set all subsequent points to where the waypoints are */
			int position = 1;
			foreach (Vector3 waypoint in waypoints) {
				lineRenderer.SetPosition(position, waypoint);
				position++;
			}
		}
	}
}
