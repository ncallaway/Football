﻿using UnityEngine;
using System.Collections.Generic;

// TODO: Break this up into several classes:
// RoutePlanner to set a series of waypoints
// RouteRunner to execute those waypoints and manuevers
// Something to reset the wide receiver
public class RouteRunner : MonoBehaviour
{
	// Publicly Exposed Values
	public bool HasTarget { get { return routeSegmentTarget.HasValue; } }
	public Vector3 ToTarget { get { return routeSegmentTarget.Value - transform.position; } }

	public bool HasWait { get { return routeSegmentWait.HasValue; } }

	public float WaitRemaining { get { return routeSegmentWait.Value - Time.time; }}

	// Route Runner Characteristics
	private const float SqrTargetEpsilon = 0.3f;

	public float acceleration = 2;
    public float speed = 8;
    public float rotationSpeed = 600f;

	// Necessary Components
	public GameObject hands;

	// RouteRunner State
	private float currentSpeed = 0;
	private Route route;
	private Vector3 initialPosition = Vector3.zero;
	private Quaternion initialRotation = Quaternion.identity;

	// Route State
	private bool startedRoute = false;
	private bool finishedRoute = false;
	private int routeSegmentStep = 0;
	private Vector3 downfield;

	// Route Segment State
	private Vector3? routeSegmentTarget = null;
	private float? routeSegmentWait = null;

	public List<Vector3> Waypoints {
		get {
			List<Vector3> waypoints = new List<Vector3>();
			if (!finishedRoute) {
				int startingSegment = routeSegmentStep;

				if (HasTarget) {
					waypoints.Add(routeSegmentTarget.Value);
					startingSegment++;
				}

				for (int nextSegmentStep = startingSegment; nextSegmentStep < this.route.Segments.Count; nextSegmentStep++) {
					RouteSegment nextSegment = this.route.Segments[nextSegmentStep];
					if (nextSegment.Type == RouteSegment.SegmentType.RUN) {
						Vector3 lastWaypoint = transform.position;
						if (waypoints.Count > 0) {
							lastWaypoint = waypoints[waypoints.Count - 1];
						}

						waypoints.Add(GetSegmentTargetStartingFrom(nextSegment, lastWaypoint));
					}
				}
			}
			return waypoints;
		}
	}

    void Start()
	{
		this.route = new ShortPost();
		initialPosition = transform.localPosition;
        initialRotation = transform.localRotation;
		downfield = transform.forward;

		Events.EventBus.Listen(Events.EventType.SNAP, OnSnap);
	}

    public void Reset()
    {
        transform.localPosition = initialPosition;
        transform.localRotation = initialRotation;
		downfield = transform.forward;

		currentSpeed = 0;
		startedRoute = false;
		finishedRoute = false;
		routeSegmentStep = 0;

		routeSegmentTarget = null;
		routeSegmentWait = null;

		this.hands.GetComponent<Renderer>().enabled = false;
		this.hands.GetComponent<Collider>().enabled = false;
    }

	/**
	 * Set the route that will be executed next
	 */
	public void SetRoute(Route route) {
		this.route = route;
	}

	/**
	 * Run the given route, by executing each of its route segments
	 */
	public void RunRoute() {
		startedRoute = true;
		StartFirstRouteSegment();
	}

	private void OnSnap(Events.Event e) {
		RunRoute();
	}

	// Update is called once per frame
    void Update()
    {
		if (startedRoute && !finishedRoute)
        {
			if (IsRouteSegmentComplete()) {
				StartNextRouteSegment();
			}

			if (!finishedRoute) {
				if (HasTarget) {
					RotateToTarget();
					MoveToTarget();
				}
			}
        }
    }

	/**
	 * Determine if the current route segment that's being execute has finished
	 */
	private bool IsRouteSegmentComplete() {
		// Routes with Targets
		if (HasTarget) {
			float sqrDistanceToTarget = ToTarget.sqrMagnitude;
			return (sqrDistanceToTarget < SqrTargetEpsilon);
		} else if (HasWait) {
			return WaitRemaining <= 0;
		} else {
			return true;
		}
	}

	/**
	 * Setup the next route segment to be run. If all route segments are completed this will finish
	 * the route's execution
	 */
	private void StartNextRouteSegment() {
		this.routeSegmentStep++;
		SetupRouteSegment(this.routeSegmentStep);
	}

	/**
	 * Setup the first route segment to be run. If there are no route segments to run this will finish
	 * the route's exectuion.
	 */
	private void StartFirstRouteSegment() {
		this.routeSegmentStep = 0;
		SetupRouteSegment(this.routeSegmentStep);
	}

	private void SetupRouteSegment(int segmentStep) {
		if (segmentStep >= this.route.Segments.Count) {
			CompleteRoute();
			return;
		}

		RouteSegment currentSegment = this.route.Segments[segmentStep];

		routeSegmentTarget = null;

		if (currentSegment.Type == RouteSegment.SegmentType.RUN) {
			routeSegmentTarget = GetSegmentTargetStartingFrom(currentSegment, this.transform.position);
		} else if (currentSegment.Type == RouteSegment.SegmentType.HANDS_ON) {
			this.hands.GetComponent<Renderer>().enabled = true;
			this.hands.GetComponent<Collider>().enabled = true;
		} else if (currentSegment.Type == RouteSegment.SegmentType.HANDS_OFF) {
			this.hands.GetComponent<Renderer>().enabled = false;
			this.hands.GetComponent<Collider>().enabled = false;
		} else if (currentSegment.Type == RouteSegment.SegmentType.WAIT) {
			routeSegmentWait = Time.time + currentSegment.WaitDuration;
		}
	}

	private Vector3 GetSegmentTargetStartingFrom(RouteSegment routeSegment, Vector3 startingPosition) {
		Quaternion downfieldTransormation = Quaternion.FromToRotation (Vector3.forward, downfield);
		Vector3 offset = downfieldTransormation * routeSegment.TargetOffset;

		return startingPosition + offset;
	}

	private void CompleteRoute() {
		Debug.Log("This route is complete! There were " + this.route.Segments.Count + " steps.");
		this.finishedRoute = true;
	}

    private void RotateToTarget()
    {
		float step = rotationSpeed * Time.deltaTime;
		transform.rotation = Quaternion.RotateTowards (transform.rotation, Quaternion.LookRotation(ToTarget), step);
    }

    private void MoveToTarget()
	{
		transform.position += ToTarget.normalized * Time.deltaTime * currentSpeed;

		if (currentSpeed < speed) {
			currentSpeed += Time.deltaTime*acceleration;
		}
	}
}
