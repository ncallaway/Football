﻿using UnityEngine;
using System;
using System.Collections.Generic;

/**
 * This base class for a Route, which represents a pattern that a receive will run. The
 * RouteRunner object is used to execute a specific Route
 */
public abstract class Route
{
	public String Name { get { return name; } }
	public float Difficulty { get { return difficulty; } }
	public float ExpectedDistance { get { return expectedDistance; } }
	public float MinimumDistance { get { return minimumDistance; } }

	private String name;
	private float difficulty;
	private float expectedDistance;
	private float minimumDistance;

	protected Route(String name, float difficulty, float expectedDistance, float minimumDistance) {
		this.name = name;
		this.difficulty = difficulty;
		this.expectedDistance = expectedDistance;
		this.minimumDistance = minimumDistance;
	}

	protected List<RouteSegment> segments;
	public List<RouteSegment> Segments { get { return segments; } }

	protected Vector3 Downfield(float distance) {
		return Vector3.forward * distance;
	}

	protected Vector3 DownfieldFade(float distance) {
		Vector3 fade = new Vector3 (0.25f, 0, 1).normalized;
		return fade * distance;
	}

	protected Vector3 DiagonalIn(float distance) {
		Vector3 diagonal = new Vector3 (-1, 0, 1).normalized;
		return diagonal * distance;
	}

	protected Vector3 DiagonalOut(float distance) {
		Vector3 diagonal = new Vector3 (1, 0, 1).normalized;
		return diagonal * distance;
	}

	protected Vector3 UpfieldDiagonalIn(float distance) {
		Vector3 diagonal = new Vector3 (-1, 0, -1).normalized;
		return diagonal * distance;
	}

	protected Vector3 Out(float distance) {
		Vector3 diagonal = Vector3.right;
		return diagonal * distance;
	}

	protected Vector3 In(float distance) {
		Vector3 diagonal = Vector3.left;
		return diagonal * distance;
	}
}

/**
 * ShortPost represents a quick-slant style short post where the receiver cuts 5-yards downfield
 * in a 45-degree diagonal across the field
 */
public class ShortPost : Route
{
	public ShortPost () : base("Short Post", 2, 10, 0)
	{
		this.segments = new List<RouteSegment>
		{
			RouteSegment.Run(Downfield(5)),
			RouteSegment.HandsOn(),
			RouteSegment.Run(DiagonalIn(12)),
			RouteSegment.HandsOff(),
			RouteSegment.Run(DiagonalIn(3))
		};
	}
}

/**
 * Short Hook represents a quick hook where the receiver runs 7 yards downfield then turns in toward the
 * quarterback.
 */
public class ShortCorner : Route
{
	public ShortCorner() : base("Short Corner", 6, 10, 0)
	{
		this.segments = new List<RouteSegment>
		{
			RouteSegment.Run(Downfield(5)),
			RouteSegment.HandsOn(),
			RouteSegment.Run(DiagonalOut(12)),
			RouteSegment.HandsOff(),
			RouteSegment.Run(DiagonalOut(3))
		};
	}
}

/**
 * Short Out represents a quick out 5-yards downfield and heads directly for the sidelines.
 */
public class ShortOut : Route
{
	public ShortOut() : base("Short Out", 4, 5, 0)
	{
		this.segments = new List<RouteSegment>
		{
			RouteSegment.Run(Downfield(5)),
			RouteSegment.HandsOn(),
			RouteSegment.Run(Out(10)),
			RouteSegment.HandsOff(),
			RouteSegment.Run(Out(3))
		};
	}
}

/**
 * Short Hook represents a quick hook where the receiver runs 7 yards downfield then turns in toward the
 * quarterback.
 */
public class ShortHook : Route
{
	public ShortHook() : base("Short Hook", 2, 5, 0)
	{
		this.segments = new List<RouteSegment>
		{
			RouteSegment.Run(Downfield(7)),
			RouteSegment.HandsOn(),
			RouteSegment.Run(UpfieldDiagonalIn(2)),
			RouteSegment.Wait(1),
			RouteSegment.HandsOff(),
			RouteSegment.Wait(1)
		};
	}
}

public class ShortFade : Route
{
	public ShortFade() : base("Short Fade", 4, 15, 5)
	{
		this.segments = new List<RouteSegment>
		{
			RouteSegment.Run(DownfieldFade(5)),
			RouteSegment.HandsOn(),
			RouteSegment.Run(DownfieldFade(12)),
			RouteSegment.HandsOff(),
			RouteSegment.Run(Downfield(3))
		};
	}
}

/**
 * Shallow Drag represents a long drag across the field just past the line of scrimmage and
 * under the linebackers.
 */
public class ShallowDrag : Route
{
	public ShallowDrag() : base("Shallow Drag", 1, 3, 0)
	{
		this.segments = new List<RouteSegment>
		{
			RouteSegment.Run(DiagonalIn(5)),
			RouteSegment.HandsOn(),
			RouteSegment.Run(In(25)),
			RouteSegment.HandsOff(),
			RouteSegment.Run(In(3))
		};
	}
}

/**
 * Short In represents a quick out 5-yards downfield and heads directly for the sidelines.
 */
public class ShortIn : Route
{
	public ShortIn() : base("Short In", 1, 5, 0)
	{
		this.segments = new List<RouteSegment>
		{
			RouteSegment.Run(Downfield(5)),
			RouteSegment.HandsOn(),
			RouteSegment.Run(In(15)),
			RouteSegment.HandsOff(),
			RouteSegment.Run(In(3))
		};
	}
}

//============= MEDIUM ROUTES ========================

public class MediumPost : Route
{
	public MediumPost () : base("Medium Post", 5, 15, 5)
	{
		this.segments = new List<RouteSegment>
		{
			RouteSegment.Run(Downfield(10)),
			RouteSegment.HandsOn(),
			RouteSegment.Run(DiagonalIn(12)),
			RouteSegment.HandsOff(),
			RouteSegment.Run(DiagonalIn(3))
		};
	}
}

public class MediumCorner : Route
{
	public MediumCorner() : base("Medium Corner", 9, 15, 5)
	{
		this.segments = new List<RouteSegment>
		{
			RouteSegment.Run(Downfield(10)),
			RouteSegment.HandsOn(),
			RouteSegment.Run(DiagonalOut(12)),
			RouteSegment.HandsOff(),
			RouteSegment.Run(DiagonalOut(3))
		};
	}
}

/**
 * Short Out represents a quick out 5-yards downfield and heads directly for the sidelines.
 */
public class MediumOut : Route
{
	public MediumOut() : base("Medium Out", 5, 10, 0)
	{
		this.segments = new List<RouteSegment>
		{
			RouteSegment.Run(Downfield(10)),
			RouteSegment.HandsOn(),
			RouteSegment.Run(Out(10)),
			RouteSegment.HandsOff(),
			RouteSegment.Run(Out(3))
		};
	}
}

/**
 * Short Hook represents a quick hook where the receiver runs 7 yards downfield then turns in toward the
 * quarterback.
 */
public class MediumHook : Route
{
	public MediumHook() : base("Medium Hook", 6, 10, 0)
	{
		this.segments = new List<RouteSegment>
		{
			RouteSegment.Run(Downfield(12)),
			RouteSegment.HandsOn(),
			RouteSegment.Run(UpfieldDiagonalIn(2)),
			RouteSegment.Wait(1),
			RouteSegment.HandsOff(),
			RouteSegment.Wait(1)
		};
	}
}

public class MediumFade : Route
{
	public MediumFade() : base("Medium Fade", 8, 20, 5)
	{
		this.segments = new List<RouteSegment>
		{
			RouteSegment.Run(DownfieldFade(12)),
			RouteSegment.HandsOn(),
			RouteSegment.Run(DownfieldFade(18)),
			RouteSegment.HandsOff(),
			RouteSegment.Run(Downfield(3))
		};
	}
}

/**
 * Shallow Drag represents a long drag across the field just past the line of scrimmage and
 * under the linebackers.
 */
public class MediumDrag : Route
{
	public MediumDrag() : base("Medium Drag", 3, 8, 0)
	{
		this.segments = new List<RouteSegment>
		{
			RouteSegment.Run(Downfield(5)),
			RouteSegment.Run(DiagonalIn(5)),
			RouteSegment.HandsOn(),
			RouteSegment.Run(In(25)),
			RouteSegment.HandsOff(),
			RouteSegment.Run(In(3))
		};
	}
}

/**
 * Short In represents a quick out 5-yards downfield and heads directly for the sidelines.
 */
public class MediumIn : Route
{
	public MediumIn() : base("Medium In", 4, 10, 0)
	{
		this.segments = new List<RouteSegment>
		{
			RouteSegment.Run(Downfield(10)),
			RouteSegment.HandsOn(),
			RouteSegment.Run(In(15)),
			RouteSegment.HandsOff(),
			RouteSegment.Run(In(3))
		};
	}
}

//================ DEEP ROUTES ====================================

public class DeepPost : Route
{
	public DeepPost () : base("Deep Post", 10, 25, 15)
	{
		this.segments = new List<RouteSegment>
		{
			RouteSegment.Run(Downfield(15)),
			RouteSegment.HandsOn(),
			RouteSegment.Run(DiagonalIn(19)),
			RouteSegment.HandsOff(),
			RouteSegment.Run(DiagonalIn(3))
		};
	}
}

public class DeepCorner : Route
{
	public DeepCorner() : base("Deep Corner", 16, 25, 15)
	{
		this.segments = new List<RouteSegment>
		{
			RouteSegment.Run(Downfield(15)),
			RouteSegment.HandsOn(),
			RouteSegment.Run(DiagonalOut(19)),
			RouteSegment.HandsOff(),
			RouteSegment.Run(DiagonalOut(3))
		};
	}
}

/**
 * Short Out represents a quick out 5-yards downfield and heads directly for the sidelines.
 */
public class DeepOut : Route
{
	public DeepOut() : base("Deep Out", 8, 15, 10)
	{
		this.segments = new List<RouteSegment>
		{
			RouteSegment.Run(Downfield(15)),
			RouteSegment.HandsOn(),
			RouteSegment.Run(Out(10)),
			RouteSegment.HandsOff(),
			RouteSegment.Run(Out(3))
		};
	}
}

/**
 * Short Hook represents a quick hook where the receiver runs 7 yards downfield then turns in toward the
 * quarterback.
 */
public class DeepHook : Route
{
	public DeepHook() : base("Deep Hook", 12, 15, 10)
	{
		this.segments = new List<RouteSegment>
		{
			RouteSegment.Run(Downfield(17)),
			RouteSegment.HandsOn(),
			RouteSegment.Run(UpfieldDiagonalIn(2)),
			RouteSegment.Wait(1),
			RouteSegment.HandsOff(),
			RouteSegment.Wait(1)
		};
	}
}

public class DeepFade : Route
{
	public DeepFade() : base("Deep Fade", 16, 25, 15)
	{
		this.segments = new List<RouteSegment>
		{
			RouteSegment.Run(DownfieldFade(15)),
			RouteSegment.HandsOn(),
			RouteSegment.Run(DownfieldFade(20)),
			RouteSegment.HandsOff(),
			RouteSegment.Run(Downfield(3))
		};
	}
}

/**
 * Short In represents a quick out 5-yards downfield and heads directly for the sidelines.
 */
public class DeepIn : Route
{
	public DeepIn() : base("Deep In", 7, 15, 7)
	{
		this.segments = new List<RouteSegment>
		{
			RouteSegment.Run(Downfield(15)),
			RouteSegment.HandsOn(),
			RouteSegment.Run(In(15)),
			RouteSegment.HandsOff(),
			RouteSegment.Run(In(3))
		};
	}
}