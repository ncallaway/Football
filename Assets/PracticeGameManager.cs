﻿using UnityEngine;
using System.Collections;

using Events;

// Responsibilities:
// 1 - Setup the overall game
// 4 - Setup each individual play
// 7 - Position Game Objects
public class PracticeGameManager : MonoBehaviour {

    public float difficultyPerYard = 0.1f;

    public GameState HotRouteState { get { return hotRouteState; } }

    private GameState hotRouteState = new GameState();

    private bool listening = false;

    // Get rid of these fuckin' things.
    public RouteRunner runner;
    public Football football;

	private Route nextRoute;

    private int[] startingPositions = new int[] { 90, 80, 70, 50, 30, 10 };

	void Start () {
        EventBus.Listen(Events.EventType.PICKUP_FOOTBALL, OnPickupFootball);
        EventBus.Listen(Events.EventType.MENU_BUTTON, OnMenuButton);

        StartCoroutine(InitializeAfter(0.01f));
	}

    public void Initialize()
    {
        hotRouteState.ResetGameState();
        NewDown();
    }

    private void NewDown()
    {
        // Setup the new route
        Route route = RouteSelector.SelectRoute(hotRouteState.DistanceToEndzone, hotRouteState.Difficulty);
        hotRouteState.RouteSetup(route);

        // Start "listening"
        listening = true;

        // Let others known to prepare for a new down
        Events.EventBus.Dispatch(Events.Event.NewDown());
    }

    private void Snap() {
        Events.EventBus.Dispatch(Events.Event.Snap());
        hotRouteState.RouteStarted();
	}

    IEnumerator InitializeAfter(float time)
    {
        yield return new WaitForSeconds(time);

        Initialize();
    }

    IEnumerator NewDownAfer(float time)
    {
        yield return new WaitForSeconds(time);

        NewDown();
    }

	// Update is called once per frame
	void Update () {
        if (listening)
        {
            if (football.WasComplete)
            {
                listening = false;

                Events.EventBus.Dispatch(Events.Event.CompletePass());

                float fieldPosition = runner.transform.position.z;

                float yardsGained = hotRouteState.CurrentFieldPosition - hotRouteState.PreviousFieldPosition;
                float difficultyIncrease = yardsGained * difficultyPerYard;

                hotRouteState.AddCompletePass(fieldPosition);
                hotRouteState.IncreaseDifficulty(difficultyIncrease);

                /* We give a little grace here, because whenever you get that close it feels like you scored,
                 * and it sucks to have to take a really short play
                 */
                if (hotRouteState.DistanceToEndzone < 1f) {
                    Events.EventBus.Dispatch(Events.Event.Touchdown());
                    hotRouteState.AddTouchdown();
                    hotRouteState.AddDropRemaining();

                    /* Get new starting field position */
                    float startForNextDrive;
                    if (hotRouteState.TouchdownsScored >= startingPositions.Length) {
                        startForNextDrive = startingPositions[startingPositions.Length-1];
                    } else {
                        startForNextDrive = startingPositions[hotRouteState.TouchdownsScored];
                    }
                    hotRouteState.StartNewDriveFrom(startForNextDrive);

                    StartCoroutine(NewDownAfer(3));
                } else {
                    StartCoroutine(NewDownAfer(1));
                }

            }

            if (football.WasIncomplete)
            {
                listening = false;

                Events.EventBus.Dispatch(Events.Event.IncompletePass());
                hotRouteState.AddIncompletePass();

                if (!hotRouteState.IsGameOver) {
                    StartCoroutine(NewDownAfer(1));
                }

            }
        }

        // Treat Enter Key like MENU button press
        if (Input.GetKeyUp(KeyCode.Return)) {
            OnMenu();
        }
	}

    private void OnPickupFootball(Events.Event e) {
        Snap();
    }

    private void OnMenuButton(Events.Event e) {
        OnMenu();
    }

    void OnMenu() {
        if (hotRouteState.IsGameOver) {
            hotRouteState.ResetGameState();
            StartCoroutine(NewDownAfer(1));
        }
    }
}
