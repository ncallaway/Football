﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Valve.VR;

using Events;

/**
 * Grabber allows the player to grab and throw any footballs in the screne. The Grabber is also responsible for providing
 * haptic feedback to the player when they grab and throw the football.
 */
public class ControllerGrabber : MonoBehaviour {
    private static readonly EVRButtonId TRIGGER = EVRButtonId.k_EButton_SteamVR_Trigger;
    private static readonly EVRButtonId MENU = EVRButtonId.k_EButton_ApplicationMenu;

    private SteamVR_Controller.Device controller { get { return SteamVR_Controller.Input((int)trackedObj.index); } }
    private SteamVR_TrackedObject trackedObj;
    private ControllerHaptics haptics;
    private Football grabbableFootball;
    private Football attachedFootball;

	void Start () {
        trackedObj = GetComponent<SteamVR_TrackedObject>();
        haptics = GetComponent<ControllerHaptics>();
	}

	void Update () {
	    if (controller == null)
        {
            Debug.Log("Controller not initailized");
            return;
        }

        // Haptic Feedback
        if (this.grabbableFootball != null && attachedFootball == null) {
            haptics.Vibrate(0.1f, 0.33f, 0.05f);
        }

        // Pickup the football
        if (controller.GetPressDown(TRIGGER) && grabbableFootball != null && attachedFootball == null)
        {
            attachedFootball = grabbableFootball;
			EventBus.Dispatch(Events.Event.PickupFootball(attachedFootball));

            Vector3 attachmentPosition = new Vector3(0, 0, 0.08f);
            Quaternion attachmentOrientation = Quaternion.Euler(341f, 355f, 97f);
            attachedFootball.AttachTo(transform, attachmentPosition, attachmentOrientation);
        }

        // Throw the football
        if (controller.GetPressUp(TRIGGER) && grabbableFootball != null)
        {
            // Haptic Feedback
            haptics.Vibrate(0.125f, 0.75f, 0.01f, true);

            Vector3 launchVelocity = 1.4f * controller.velocity;
            attachedFootball.Throw(launchVelocity);
            attachedFootball = null;
        }

        // Menu Button
        if (controller.GetPressUp(MENU))
        {
            EventBus.Dispatch(Events.Event.MenuButton());
        }
    }

    // Track whether we can currently grab a football
    void OnTriggerEnter(Collider collider)
    {
        Football football = collider.GetComponent<Football>();
        if (football != null) {
            grabbableFootball = football;
        }
    }

    void OnTriggerExit(Collider collider)
    {
        Football football = collider.GetComponent<Football>();
        if (football == grabbableFootball) {
            grabbableFootball = null;
        }
    }
}
