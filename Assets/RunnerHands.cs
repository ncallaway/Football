﻿using UnityEngine;
using System.Collections;

public class RunnerHands : MonoBehaviour {

	public Football football;
	public IKHands ikProxy;

	private void Update () {
		if (football.WasComplete) {
			football.GetComponent<Rigidbody>().isKinematic = true;
			football.transform.parent = transform;
			football.transform.localPosition = new Vector3(0.258f, 0.568f, 0.738f);
			football.transform.localRotation = Quaternion.Euler(270, 90, 0);
		}

		// Point the hands at an incoming football
		Vector3 fbPosition = football.transform.position;
		if(football.IsThrown && DistanceToFootball() < 5) {
			// Vector3 rightTarget = Vector3.MoveTowards(ikProxy.RightHandPosition, fbPosition, 1*Time.deltaTime);
			// Vector3 leftTarget = Vector3.MoveTowards(ikProxy.LeftHandPosition, fbPosition, 1*Time.deltaTime);
			Vector3 rightTarget = fbPosition;
			Vector3 leftTarget = fbPosition;

			ikProxy.RightHandTarget = rightTarget;
			ikProxy.LeftHandTarget = fbPosition;
		} else if (football.WasComplete) {
			ikProxy.RightHandTarget = fbPosition + (Vector3)(transform.localToWorldMatrix * new Vector3(0.18f, 0f, -0.03f));
			ikProxy.LeftHandTarget = null;
		} else {
			ikProxy.RightHandTarget = null;
			ikProxy.LeftHandTarget = null;
		}
	}

	private float DistanceToFootball() {
		return (football.transform.position - transform.position).magnitude;
	}
}
