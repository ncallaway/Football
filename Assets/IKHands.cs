﻿using UnityEngine;
using System.Collections;

public class IKHands : MonoBehaviour {
	private Animator animator;

	public Vector3 RightHandPosition { get { return animator.GetBoneTransform(HumanBodyBones.RightHand).position; } }
	public Vector3 LeftHandPosition { get { return animator.GetBoneTransform(HumanBodyBones.LeftHand).position; } }
	public Vector3? RightHandTarget { get { return rightHandTarget; } set { rightHandTarget = value; } }
	public Vector3? LeftHandTarget { get { return leftHandTarget; } set { leftHandTarget = value; } }

	private Vector3? rightHandTarget;
	private Vector3? leftHandTarget;

	private void Start() {
		animator = GetComponent<Animator>();
	}

	private void OnAnimatorIK()
    {
		if (rightHandTarget.HasValue) {
			animator.SetIKPositionWeight(AvatarIKGoal.RightHand, 1);
			animator.SetIKPosition(AvatarIKGoal.RightHand, rightHandTarget.Value);
		} else {
			animator.SetIKPositionWeight(AvatarIKGoal.RightHand, 0);
		}

		if (leftHandTarget.HasValue) {
			animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, 1);
			animator.SetIKPosition(AvatarIKGoal.LeftHand, leftHandTarget.Value);
		} else {
			animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, 0);
		}
    }
}
