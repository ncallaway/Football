Assets
    Models\
        {AssetName}
            {AssetName}.fbx
            Textures\
                WR_Diffuse_Texture.png
            Bakes\
                WR_NOR.png
                WR_AO.png

    Textures
        TeamLogoSeattleBlues.png

SourceAssets
    Models\
        {AssetName}\
            {AssetName}.blend
            {AssetName}.fbx
            Textures\
                {AssetShort}_Diffuse_{TextureName}.psd
                {AssetShort}_Diffuse_{TextureName}.png
            Bakes
                {AssetShort}_NOR.png
                {AssetShort}_AO.png

    Textures\
        {AssetName}.psd
        {AssetName}.png